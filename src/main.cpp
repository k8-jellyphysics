/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "jelly/JellyPhysics.h"
#include "videolib/videolib.h"


////////////////////////////////////////////////////////////////////////////////
static float scale_x = 19.0f;
static float scale_y = 20.0f;
static float center_x = -21.0f;
static float center_y = -20.0f;


static inline int x2s (float x) { return (x-center_x)*scale_x; }
static inline int y2s (float y) { return SCR_WIDTH-(y-center_y)*scale_y; }

static inline float s2x (int x) { return ((float)x/scale_x)+center_x; }
static inline float s2y (int y) { return ((float)(SCR_WIDTH-y)/scale_y)+center_y; }


static void dll (float x0, float y0, float x1, float y1, Uint32 clr) {
  x0 = x2s(x0);
  y0 = y2s(y0);
  x1 = x2s(x1);
  y1 = y2s(y1);
  draw_line(x0, y0, x1, y1, clr);
}


////////////////////////////////////////////////////////////////////////////////
using namespace JellyPhysics;


////////////////////////////////////////////////////////////////////////////////
// simple inherited classes, to add gravity force
class FallingBody : public SpringBody {
public:
  FallingBody (
    World *w, ClosedShape &s, float massPerPoint,
    float edgeSpringK, float edgeSpringDamp,
    const Vector2 &pos, float angle,
    const Vector2 &scale, bool kinematic=false
  ) : SpringBody(w, s, massPerPoint, edgeSpringK, edgeSpringDamp, pos, angle, scale, kinematic) {}

  FallingBody (
    World *w, const ClosedShape &shape, float massPerPoint,
    float shapeSpringK, float shapeSpringDamp,
    float edgeSpringK, float edgeSpringDamp,
    const Vector2 &pos, float angleinRadians,
    const Vector2 &scale, bool kinematic=false
  ) : SpringBody(w, shape, massPerPoint, shapeSpringK, shapeSpringDamp, edgeSpringK, edgeSpringDamp, pos, angleinRadians, scale, kinematic) {}

  virtual void accumulateExternalForces () {
    // gravity!
    for (unsigned int i = 0; i < mPointMasses.size(); ++i) mPointMasses[i].Force += Vector2(0.0f, -9.8f*mPointMasses[i].mass());
  }
};


class FallingPressureBody : public PressureBody {
public:
  FallingPressureBody (
    World *w, const ClosedShape &s, float mpp,
    float gasPressure, float shapeK, float shapeD,
    float edgeK, float edgeD,
    const Vector2 &pos, float angleInRadians, const Vector2 &scale,
    bool kinematic=false) : PressureBody(w, s, mpp, gasPressure, shapeK, shapeD, edgeK, edgeD, pos, angleInRadians, scale, kinematic) {}

  virtual void accumulateExternalForces () {
    // gravity!
    for (unsigned int i = 0; i < mPointMasses.size(); ++i) mPointMasses[i].Force += Vector2(0.0f, -9.8f*mPointMasses[i].mass());
  }
};


////////////////////////////////////////////////////////////////////////////////
static World *mWorld = NULL;


static void createI (float x, float y, int random_angle) {
  // this is a complex body, in the shape of a capital "I", and connected with many internal springs.
  ClosedShape shape;
  shape.begin();
  shape.addVertex(Vector2(-1.5f,  2.0f));
  shape.addVertex(Vector2(-0.5f,  2.0f));
  shape.addVertex(Vector2( 0.5f,  2.0f));
  shape.addVertex(Vector2( 1.5f,  2.0f));
  shape.addVertex(Vector2( 1.5f,  1.0f));
  shape.addVertex(Vector2( 0.5f,  1.0f));
  shape.addVertex(Vector2( 0.5f, -1.0f));
  shape.addVertex(Vector2( 1.5f, -1.0f));
  shape.addVertex(Vector2( 1.5f, -2.0f));
  shape.addVertex(Vector2( 0.5f, -2.0f));
  shape.addVertex(Vector2(-0.5f, -2.0f));
  shape.addVertex(Vector2(-1.5f, -2.0f));
  shape.addVertex(Vector2(-1.5f, -1.0f));
  shape.addVertex(Vector2(-0.5f, -1.0f));
  shape.addVertex(Vector2(-0.5f,  1.0f));
  shape.addVertex(Vector2(-1.5f,  1.0f));
  shape.finish();

  FallingBody *body = new FallingBody(mWorld, shape, 1.0f, 150.0f, 5.0f, 300.0f, 15.0f, Vector2(x, y), (random_angle ? degToRad((float)rand()/RAND_MAX*360.0f) : 0.0f), Vector2::One);
  body->addInternalSpring(0, 14, 300.0f, 10.0f);
  body->addInternalSpring(1, 14, 300.0f, 10.0f);
  body->addInternalSpring(1, 15, 300.0f, 10.0f);
  body->addInternalSpring(1, 5, 300.0f, 10.0f);
  body->addInternalSpring(2, 14, 300.0f, 10.0f);
  body->addInternalSpring(2, 5, 300.0f, 10.0f);
  body->addInternalSpring(1, 5, 300.0f, 10.0f);
  body->addInternalSpring(14, 5, 300.0f, 10.0f);
  body->addInternalSpring(2, 4, 300.0f, 10.0f);
  body->addInternalSpring(3, 5, 300.0f, 10.0f);
  body->addInternalSpring(14, 6, 300.0f, 10.0f);
  body->addInternalSpring(5, 13, 300.0f, 10.0f);
  body->addInternalSpring(13, 6, 300.0f, 10.0f);
  body->addInternalSpring(12, 10, 300.0f, 10.0f);
  body->addInternalSpring(13, 11, 300.0f, 10.0f);
  body->addInternalSpring(13, 10, 300.0f, 10.0f);
  body->addInternalSpring(13, 9, 300.0f, 10.0f);
  body->addInternalSpring(6, 10, 300.0f, 10.0f);
  body->addInternalSpring(6, 9, 300.0f, 10.0f);
  body->addInternalSpring(6, 8, 300.0f, 10.0f);
  body->addInternalSpring(7, 9, 300.0f, 10.0f);
}


static void createBall (float x, float y) {
  // pressure body: similar to a SpringBody, but with internal pressurized gas to help maintain shape
  ClosedShape ball;
  ball.begin();
  for (int i = 0; i < 360; i += 20) {
    ball.addVertex(Vector2(cosf(degToRad((float)-i)), sinf(degToRad((float)-i))));
  }
  ball.finish();
  new FallingPressureBody(mWorld, ball, 1.0f, 40.0f, 10.0f, 1.0f, 300.0f, 20.0f, Vector2(x, y), 0, Vector2::One);
}


static void initialize (void) {
  // create physics world
  mWorld = new World(1.0f/600.0f);
  // static ground object
  // all Closed Shape objects are assumed to be a list of lines going from point to point, with the last point
  // connecting back to the first point to close the shape. this is a simple rectangle to represent the ground for this demo.
  // ClosedShape objects are automatically "centered" when you call finish(), and that center becomes the center when
  // setting the position of the object.
  ClosedShape groundShape;
  groundShape.begin();
  /*
  groundShape.addVertex(Vector2(-20.0f, -1.0f));
  groundShape.addVertex(Vector2(-20.0f,  1.0f));
  groundShape.addVertex(Vector2( 20.0f,  1.0f));
  groundShape.addVertex(Vector2( 20.0f, -1.0f));
  */
  groundShape.addVertex(Vector2(-20.0f, -1.0f));
  groundShape.addVertex(Vector2(-20.0f,  3.0f));
  groundShape.addVertex(Vector2(-10.0f,  0.6f));
  groundShape.addVertex(Vector2(  0.0f,  0.3f));
  groundShape.addVertex(Vector2( 10.0f,  0.6f));
  groundShape.addVertex(Vector2( 20.0f,  1.0f));
  groundShape.addVertex(Vector2( 20.0f, -1.0f));
  groundShape.finish();
  // make the body
  // creating the body. since this is a static body, we can use the base class Body.
  // setting the mass per point to 0.0f or INFINITY makes it immobile / static.
  new Body(mWorld, groundShape, INFINITY, Vector2(0.0f, -5.0f), 0.0f, Vector2::One, false);
}


////////////////////////////////////////////////////////////////////////////////
static void update (int elapsed_ms) {
  sdl_frame_changed = 1;
  // UPDATE the physics!
  for (int i = 0; i < 10; ++i) mWorld->update();
  // remove out-of-bounds bodies
  for (int f = mWorld->bodyCount()-1; f > 0; --f) {
    Body *b = mWorld->body(f);
    Vector2 p = b->getDerivedPosition();
    Vector2 v = b->getDerivedVelocity();
    //fprintf(stderr, "(%f,%f); v.X=%f; v.Y=%f\n", p.X, p.Y, v.X, v.Y);
    if (p.Y < -8.0f && v.Y <= 0.0f) {
      // kill it with fire!
      delete b;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static int ctrl_down = 0;
static int shift_down = 0;


static void process_keyup (SDL_KeyboardEvent *ev) {
  if (ev->keysym.sym == SDLK_LCTRL) ctrl_down = 0;
  if (ev->keysym.sym == SDLK_LSHIFT) shift_down = 0;
}


static void process_keydown (SDL_KeyboardEvent *ev) {
  if (ev->keysym.sym == SDLK_RETURN && (ev->keysym.mod&KMOD_ALT)) { switch_fullscreen(); return; }
  if (ev->keysym.sym == SDLK_ESCAPE) { post_quit_message(); return; }
  if (ev->keysym.sym == SDLK_LCTRL) ctrl_down = 1;
  if (ev->keysym.sym == SDLK_LSHIFT) shift_down = 1;
  if (ev->keysym.sym == SDLK_SPACE) {
    /* clear world */
    while (mWorld->bodyCount() > 1) delete mWorld->body(mWorld->bodyCount()-1);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void process_mousedown (SDL_MouseButtonEvent *ev) {
  if (ev->button == SDL_BUTTON_LEFT) {
    float x = s2x(ev->x);
    float y = s2y(ev->y);
    //fprintf(stderr, "x=%.15g; y=%.15g\n", x, y);
    ClosedShape shape;
    shape.begin();
    shape.addVertex(Vector2(-1.0f, -1.0f));
    shape.addVertex(Vector2(-1.0f,  1.0f));
    shape.addVertex(Vector2( 1.0f,  1.0f));
    shape.addVertex(Vector2( 1.0f, -1.0f));
    shape.finish();

    FallingBody *body = new FallingBody(mWorld, shape, 1.0f, 300.0f, 10.0f, Vector2(x, y), (shift_down ? 0.0f : degToRad((float)rand()/RAND_MAX*360.0f)), Vector2::One);
    body->addInternalSpring(0, 2, 400.0f, 12.0f);
    body->addInternalSpring(1, 3, 400.0f, 12.0f);
    return;
  }

  if (ev->button == SDL_BUTTON_RIGHT) {
    float x = s2x(ev->x);
    float y = s2y(ev->y);
    createI(x, y, ctrl_down);
    return;
  }

  if (ev->button == SDL_BUTTON_MIDDLE) {
    float x = s2x(ev->x);
    float y = s2y(ev->y);
    createBall(x, y);
    return;
  }
}


static void rebuild (void) {
  memset(sdl_vscr, 0, SCR_WIDTH*SCR_HEIGHT*sizeof(sdl_vscr[0]));
  for (int f = 0; f < mWorld->bodyCount(); ++f) {
    Body *b = mWorld->body(f);
    Vector2List vrt = b->getVerticiesInWorld();
    unsigned int pn = vrt.size()-1;
    vl_polymod_start();
    for (unsigned int vn = 0; vn < vrt.size(); pn = vn++) {
      //fprintf(stderr, "vn=%u; x=%d; y=%d\n", vn, x2s(vrt[vn].X), y2s(vrt[vn].Y));
      //dll(vrt[pn].X, vrt[pn].Y, vrt[vn].X, vrt[vn].Y, rgb2col(255, 255, 255));
      vl_polymod_add_vertex(x2s(vrt[vn].X), y2s(vrt[vn].Y));
    }
    vl_polymod_end();
    vl_polymod_fill_scr(rgb2col(255, 255, 255*(f+1)/mWorld->bodyCount()));
  }
  //
  {
    static char buf[128];
    snprintf(buf, sizeof(buf), "body count: %d", mWorld->bodyCount());
    //draw_str_pr(1, 1, "Meow!WO-OIl!", rgb2col(255, 255, 0), rgba2col(0, 0, 200, 200));
    draw_str(1, 1, buf, rgb2col(255, 255, 0), mWorld->bodyCount());
  }
}



////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  sdl_mag2x = 0;
  videolib_args(&argc, argv);
  if (videolib_init("JellyPhysics Demo 00") < 0) {
    fprintf(stderr, "FATAL: can't initialize SDL!\n");
    exit(1);
  }
  vl_fps = 60;
  vl_rebuild = rebuild;
  vl_update = update;
  vl_process_keydown = process_keydown;
  vl_process_keyup = process_keyup;
  vl_process_mousedown = process_mousedown;
  initialize();
  vl_polymod_init();
  vl_main_loop();
  vl_polymod_deinit();
  delete mWorld;
  return 0;
}
