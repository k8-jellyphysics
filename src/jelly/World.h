/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _WORLD_H
#define _WORLD_H

#include <stdlib.h>

#include "JellyPrerequisites.h"
#include "Body.h"
#include "Vector2.h"


namespace JellyPhysics {

class CollisionCallback {
public:
  virtual bool collisionFilter (Body *bA, int bodyApm, Body *bodyB, int bodyBpm1, int bodyBpm2, Vector2 hitPt, float normalVel) { return true; }
};


class World {
public:
  struct MaterialPair {
    MaterialPair () : Collide(true), Elasticity(0.7f), Friction(0.3f), Callback(0) {}

    bool Collide;
    float Elasticity;
    float Friction;
    CollisionCallback *Callback;
  };

public:
  World (float timestep);
  ~World ();

  void killing ();

  void setWorldLimits (const Vector2 &min, const Vector2 &max);

  int addMaterial ();

  void setMaterialPairCollide (int a, int b, bool collide);
  void setMaterialPairData (int a, int b, float friction, float elasticity);
  void setMaterialPairFilterCallback (int a, int b, CollisionCallback *c);

  inline int bodyCount (void) const { return mBodyCount; }
  inline Body *body (int index) const { return (index >= 0 && index < mBodyCount ? mBodies[index] : NULL); }

  void addBody (Body *b);
  void removeBody (Body *b);

  void getClosestPointMass (const Vector2 &pt, int *bodyID, int *pmID);
  Body *getBodyContaining (const Vector2 &pt);

  void update ();

  int materialCount () const { return mMaterialCount; }

  float getPenetrationThreshold () const { return mPenetrationThreshold; }
  void setPenetrationThreshold (float val) { mPenetrationThreshold = val; }

  int penetrationCount () const { return mPenetrationCount; }

  inline float timestep () const { return mTimeStep; }

private:
  struct BodyCollisionInfo {
    void Clear () { bodyA = bodyB = 0; bodyApm = bodyBpmA = bodyBpmB = -1; hitPt = norm = Vector2::Zero; edgeD = penetration = 0.0f; }
    void Log () const {
      //printf("BCI bodyA:%d bodyB:%d bApm:%d bBpmA:%d, bBpmB:%d\n", bodyA, bodyB, bodyApm, bodyBpmA, bodyBpmB);
      //printf("  hitPt[%f][%f] edgeD:%f norm[%f][%f] penetration:%f\n",
      //     hitPt.X, hitPt.Y, edgeD, norm.X, norm.Y, penetration);
    }

    Body *bodyA;
    Body *bodyB;

    int bodyApm;
    int bodyBpmA;
    int bodyBpmB;

    Vector2 hitPt;
    float edgeD;
    Vector2 norm;
    float penetration;
  };

private:
  void pushCollision (const BodyCollisionInfo &ci);

  void updateBodyBitmask (Body *b);
  void sortBodyBoundaries ();

  void _goNarrowCheck (Body *bI, Body *bJ);
  void bodyCollide (Body *bA, Body *bB);
  void _handleCollisions ();

  void _checkAndMoveBoundary (Body::BodyBoundary *bb);
  void _removeBoundary (Body::BodyBoundary *me);
  void _addBoundaryAfter (Body::BodyBoundary *me, Body::BodyBoundary *toAfterMe);
  void _addBoundaryBefore (Body::BodyBoundary *me, Body::BodyBoundary *toBeforeMe);

  void _logMaterialCollide () const;
  void _logBoundaries () const;

private:
  Body **mBodies;
  int mBodyCount;
  int mBodyAlloted;
  AABB mWorldLimits;
  Vector2 mWorldSize;
  Vector2 mWorldGridStep;

  float mPenetrationThreshold;
  int mPenetrationCount;

  MaterialPair *mMaterialPairs;
  MaterialPair mDefaultMatPair;
  int mMaterialCount;

  BodyCollisionInfo *mCollisionList;
  int mCLCount;
  int mCLAlloted;

  float mTimeStep;
};

}


#endif
