/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _BODY_H
#define _BODY_H

#include "JellyPrerequisites.h"

#include "AABB.h"
#include "Bitmask.h"
#include "ClosedShape.h"
#include "PointMass.h"
#include "Vector2.h"


namespace JellyPhysics {

class World;

class Body {
public:
  struct BodyBoundary {
    enum BoundaryType { Begin, End, VoidMarker };

    BoundaryType type;
    float value;

    BodyBoundary *next;
    BodyBoundary *prev;

    Body *body;

    BodyBoundary () : type(Begin), value(0.0f), next(0), prev(0), body(0) {}
    BodyBoundary (Body *b, BoundaryType t, float v) : next(0), prev(0) { body = b; type = t; value = v; }

    void log () const;
  };

public:
  Body (World *w);
  Body (World *w, const ClosedShape &shape, float massPerPoint, const Vector2 &position, float angleInRadians, const Vector2 &scale, bool kinematic);
  Body (World *w, const ClosedShape &shape, std::vector<float> pointMasses, const Vector2 &position, float angleInRadians, const Vector2 &scale, bool kinematic);

  virtual ~Body ();

  void setShape (const ClosedShape &shape);

  void setMassAll (float mass);
  void setMassIndividual (int index, float mass);
  void setMassFromList (const std::vector<float> &masses);

  int getMaterial () const { return mMaterial; }
  void setMaterial (int val) { mMaterial = val; }

  void setPositionAngle (const Vector2 &pos, float angleInRadians, const Vector2 &scale);

  virtual void setKinematicPosition (const Vector2 &pos) { mDerivedPos = pos; }
  virtual void setKinematicAngle (float angleInRadians) { mDerivedAngle = angleInRadians; }
  virtual void setKinematicScale (const Vector2 &scale) { mScale = scale; }

  void derivePositionAndAngle (float elapsed);

  void updateEdgeInfo (bool forceUpdate=false);

  virtual void updateBoundaryValues (bool forceUpdate=false);

  Vector2 getDerivedPosition () const { return mDerivedPos; }
  float getDerivedAngle () const { return mDerivedAngle; }
  Vector2 getDerivedVelocity () const { return mDerivedVel; }
  float getDerivedOmega () const { return mDerivedOmega; }

  Vector2 getScale () const { return mScale; }

  virtual void accumulateInternalForces () {}
  virtual void accumulateExternalForces () {}

  void integrate (float elapsed);
  void dampenVelocity ();

  void updateAABB (float elapsed, bool forceUpdate=false);
  const AABB &getAABB () const { return mAABB; }

  bool contains (const Vector2 &pt) const;

  float getClosestPoint (const Vector2 &pt, Vector2 *hitPt, Vector2 *norm, int *pointA, int *pointB, float *edgeD) const;
  float getClosestPointOnEdge (const Vector2 &pt, int edgeNum, Vector2 *hitPt, Vector2 *norm, float *edgeD) const;
  float getClosestPointOnEdgeSquared (const Vector2 &pt, int edgeNum, Vector2 *hitPt, Vector2 *norm, float *edgeD) const;
  int getClosestPointMass (const Vector2 &pos, float *dist) const;

  int pointMassCount () const { return mPointCount; }
  PointMass *getPointMass (int index) { return &mPointMasses[index]; }

  void addGlobalForce (const Vector2 &pt, const Vector2 &force);

  bool getIsStatic () const { return mIsStatic; }
  void setIsStatic (bool val) { mIsStatic = val; }

  bool getIsKinematic () const { return mKinematic; }
  void setIsKinematic (bool val) { mKinematic = val; }

  float getVelocityDamping () const { return mVelDamping; }
  void setVelocityDamping (float val) { mVelDamping = val; }

  void *getObjectTag () const { return mObjectTag; }
  void setObjectTag (void *obj) { mObjectTag = obj; }

  bool getIgnoreMe () const { return mIgnoreMe; }
  void setIgnoreMe (bool setting) { mIgnoreMe = setting; }

  const Vector2List &getGlobalShape (void) const { return mGlobalShape; }
  const ClosedShape &getBaseShape (void) const { return mBaseShape; }

  Vector2List getVerticiesInWorld () const;
  void getVerticiesInWorld (Vector2List &outList) const;

public:
  //Bitmask mBitMaskX;
  Bitmask mBitMaskY;

  BodyBoundary mBoundStart;
  BodyBoundary mBoundEnd;

protected:
  typedef std::vector<PointMass> PointMassList;

  struct EdgeInfo {
    Vector2 dir;  // normalized direction vector for this edge
    float length; // length of the edge
    float slope;  // slope of the line described by this edge
  };

  typedef std::vector<EdgeInfo> EdgeInfoList;

protected:
  World *mWorld;
  ClosedShape mBaseShape;
  Vector2List mGlobalShape;
  PointMassList mPointMasses;
  EdgeInfoList mEdgeInfo;
  Vector2 mScale;
  Vector2 mDerivedPos;
  Vector2 mDerivedVel;
  float mDerivedAngle;
  float mDerivedOmega;
  float mLastAngle;
  AABB mAABB;
  int mMaterial;
  bool mIsStatic;
  bool mKinematic;
  void *mObjectTag;
  float mVelDamping;

  int mPointCount;
  float mInvPC;

  bool mIgnoreMe;
};

}


#endif
