/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "ClosedShape.h"

#include "VectorTools.h"


namespace JellyPhysics {

void ClosedShape::begin () {
  mLocalVertices.clear();
}


int ClosedShape::addVertex (const Vector2 &vec) {
  mLocalVertices.push_back(vec);
  return mLocalVertices.size();
}


void ClosedShape::finish (bool recenter) {
  if (recenter) {
    // find the average location of all of the vertices, this is our geometrical center
    Vector2 center = Vector2::Zero;
    unsigned int c = mLocalVertices.size();
    for (unsigned int i = 0; i < c; ++i) center += mLocalVertices[i];
    center /= (float)c;
    // now subtract this from each element, to get proper "local" coordinates
    for (unsigned int i = 0; i < c; ++i) mLocalVertices[i] -= center;
  }
}


Vector2List ClosedShape::transformVertices (const Vector2 &worldPos, float angleInRadians, const Vector2 &scale) const {
  Vector2List ret = mLocalVertices;
  Vector2 v;
  unsigned int c = ret.size();
  for (unsigned int i = 0; i < c; ++i) {
    // rotate the point, and then translate
    v = ret[i]*scale;
    v = rotateVector(v, angleInRadians);
    v += worldPos;
    ret[i] = v;
  }
  return ret;
}


void ClosedShape::transformVertices (const Vector2 &worldPos, float angleInRadians, const Vector2 &scale, Vector2List &outList) const {
  float c = cosf(angleInRadians);
  float s = sinf(angleInRadians);
  Vector2List::iterator out = outList.begin();
  for (Vector2List::const_iterator it = mLocalVertices.begin(); it != mLocalVertices.end(); ++it, ++out) {
    // rotate the point, and then translate
    Vector2 v =(*it)*scale;
    v = rotateVector(v, c, s);
    v += worldPos;
    (*out) = v;
  }
}

}
