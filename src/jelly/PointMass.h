/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _POINT_MASS_H
#define _POINT_MASS_H

#include "Vector2.h"


namespace JellyPhysics {

class PointMass {
public:
  PointMass () : Position(Vector2::Zero), Velocity(Vector2::Zero), Force(Vector2::Zero), mMass(0.0f), mInfMass(true), lastElapsed(0), lastElapMass(1)  {}
  PointMass (float mass, const Vector2 &pos);

  void integrateForce (float elapsed);

  inline float mass () const { return mMass; }
  inline bool isInfMass () const { return mInfMass; }
  void setMass (float mass);

public:
  Vector2 Position;
  Vector2 Velocity;
  Vector2 Force;

protected:
  float mMass; // DO NOT CHANGE DIRECTRY W/O CHANGING mInfMass!
  bool mInfMass; /* no mass/infinite mass */

private:
  float lastElapsed;
  float lastElapMass;
};

}


#endif
