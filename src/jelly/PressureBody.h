/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _PRESSURE_BODY_H
#define _PRESSURE_BODY_H

#include "SpringBody.h"


namespace JellyPhysics {

class PressureBody : public SpringBody {
public:
  PressureBody (
    World *w, const ClosedShape &s, float mpp,
    float gasPressure, float shapeK, float shapeD,
    float edgeK, float edgeD,
    const Vector2 &pos, float angleInRadians, const Vector2 &scale,
    bool kinematic
  ) : SpringBody(w, s, mpp, shapeK, shapeD, edgeK, edgeD, pos, angleInRadians, scale, kinematic) {
    mGasAmount = gasPressure;
    mNormalList = new Vector2[mPointCount];
  }

  virtual ~PressureBody ();

  void setGasPressure (float val) { mGasAmount = val; }
  float getGasPressure () const { return mGasAmount; }

  float getVolume() const { return mVolume; }

  void accumulateInternalForces ();

protected:
  float mVolume;
  float mGasAmount;
  Vector2 *mNormalList;
};

}


#endif
