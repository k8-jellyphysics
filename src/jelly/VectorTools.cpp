/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "VectorTools.h"


namespace JellyPhysics {

bool lineIntersect (const Vector2 &ptA, const Vector2 &ptB, const Vector2 &ptC, const Vector2 &ptD, Vector2 &hitPt, float &Ua, float &Ub) {
  hitPt = Vector2::Zero;
  Ua = Ub = 0.0f;
  float denom = ((ptD.Y-ptC.Y)*(ptB.X-ptA.X))-((ptD.X-ptC.X)*(ptB.Y-ptA.Y));
  // if denom == 0, lines are parallel - being a bit generous on this one
  if (fabsf(denom) < 0.000001f) return false;
  float UaTop = ((ptD.X-ptC.X)*(ptA.Y-ptC.Y))-((ptD.Y-ptC.Y)*(ptA.X-ptC.X));
  float UbTop = ((ptB.X-ptA.X)*(ptA.Y-ptC.Y))-((ptB.Y-ptA.Y)*(ptA.X-ptC.X));
  Ua = UaTop/denom;
  Ub = UbTop/denom;
  if (Ua >= 0.0f && Ua <= 1.0f && Ub >= 0.0f && Ub <= 1.0f) {
    // these lines intersect!
    hitPt = ptA+((ptB-ptA)*Ua);
    return true;
  }
  return false;
}


bool lineIntersect (const Vector2 &ptA, const Vector2 &ptB, const Vector2 &ptC, const Vector2 &ptD, Vector2 &hitPt) {
  float Ua, Ub;
  return lineIntersect(ptA, ptB, ptC, ptD, hitPt, Ua, Ub);
}


Vector2 calculateSpringForce (
  const Vector2 &posA, const Vector2 &velA,
  const Vector2 &posB, const Vector2 &velB,
  float springD, float springK, float damping)
{
  Vector2 BtoA = posA-posB;
  float dist = BtoA.length();
  if (dist > 0.0001f) BtoA /= dist; else BtoA = Vector2::Zero;
  dist = springD-dist;
  Vector2 relVel = velA-velB;
  float totalRelVel = relVel.dotProduct(BtoA);
  //printf("calculateSpringForce: dist[%f] k[%f]  totalRelVel[%f] damp[%f]\n", dist, springK, totalRelVel, damping);
  return BtoA*((dist*springK)-(totalRelVel*damping));
}


Vector2 calculateSpringForce (
  const Vector2 &dir, float length,
  const Vector2 &velA, const Vector2 &velB,
  float springD, float springK, float damping)
{
  float dist = springD-length;
  Vector2 relVel = velA-velB;
  float totalRelVel = relVel.dotProduct(dir);
  //printf("calculateSpringForce: dist[%f] k[%f]  totalRelVel[%f] damp[%f]\n", dist, springK, totalRelVel, damping);
  return dir*((dist*springK)-(totalRelVel*damping));
}

}
