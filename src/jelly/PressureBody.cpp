/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "PressureBody.h"


namespace JellyPhysics {

PressureBody::~PressureBody () {
  delete[] mNormalList;
  //delete[] mEdgeLengthList;
}


void PressureBody::accumulateInternalForces () {
  SpringBody::accumulateInternalForces();
  // internal forces based on pressure equations.  we need 2 loops to do this.  one to find the overall volume of the
  // body, and 1 to apply forces.  we will need the normals for the edges in both loops, so we will cache them and remember them.
  mVolume = 0.0f;
  int c = mPointCount;
  for (int i = 0; i < c; ++i) {
    int prev = (i-1 >= 0 ? i-1 : c-1);
    int next = (i+1 < c ? i+1 : 0);
    PointMass& pmP = mPointMasses[prev];
    PointMass& pmI = mPointMasses[i];
    PointMass& pmN = mPointMasses[next];
    // currently we are talking about the edge from i --> j
    // first calculate the volume of the body, and cache normals as we go
    Vector2 edge1N = pmI.Position-pmP.Position;
    edge1N.makePerpendicular();
    Vector2 edge2N = pmN.Position-pmI.Position;
    edge2N.makePerpendicular();
    Vector2 norm = edge1N+edge2N;
    norm.normalise();
    float edgeL = mEdgeInfo[i].length; //edge2N.length();
    // cache normal and edge length
    mNormalList[i] = norm;
    //mEdgeLengthList[i] = edgeL;
    float xdist = fabsf(pmI.Position.X-pmN.Position.X);
    float normX = fabsf(norm.X);
    float volumeProduct = xdist*normX*edgeL;
    // add to volume
    mVolume += 0.5f*volumeProduct;
  }
  // now loop through, adding forces!
  float invVolume = 1.0f/mVolume;
  for (int i = 0; i < c; ++i) {
    int j = (i+1 < c ? i+1 : 0);
    float pressureV = invVolume*mEdgeInfo[i].length*mGasAmount;
    mPointMasses[i].Force += mNormalList[i]*pressureV;
    mPointMasses[j].Force += mNormalList[j]*pressureV;
  }
}

}
