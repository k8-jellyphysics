/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "SpringBody.h"

#include "World.h"
#include "VectorTools.h"


namespace JellyPhysics {

SpringBody::SpringBody (
  World *w, const ClosedShape &shape, float massPerPoint,
  float edgeSpringK, float edgeSpringDamp,
  const Vector2 &pos, float angleinRadians,
  const Vector2 &scale, bool kinematic
) : Body(w, shape, massPerPoint, pos, angleinRadians, scale, kinematic) {
  mShapeMatchingOn = false;
  setPositionAngle(pos, angleinRadians, scale);
  mEdgeSpringK = edgeSpringK;
  mEdgeSpringDamp = edgeSpringDamp;
  mShapeSpringK = 0.0f;
  mShapeSpringDamp = 0.0f;
  // build default springs
  _buildDefaultSprings();
}


SpringBody::SpringBody (
  World *w, const ClosedShape &shape, float massPerPoint,
  float shapeSpringK, float shapeSpringDamp,
  float edgeSpringK, float edgeSpringDamp,
  const Vector2 &pos, float angleinRadians,
  const Vector2 &scale, bool kinematic
) : Body(w, shape, massPerPoint, pos, angleinRadians, scale, kinematic) {
  setPositionAngle(pos, angleinRadians, scale);
  mShapeMatchingOn = true;
  mShapeSpringK = shapeSpringK;
  mShapeSpringDamp = shapeSpringDamp;
  mEdgeSpringK = edgeSpringK;
  mEdgeSpringDamp = edgeSpringDamp;
  // build default springs
  _buildDefaultSprings();
}


SpringBody::~SpringBody () {
}


void SpringBody::addInternalSpring (int pointA, int pointB, float springK, float damping) {
  float dist = (mPointMasses[pointB].Position-mPointMasses[pointA].Position).length();
  InternalSpring s(pointA, pointB, dist, springK, damping);
  mSprings.push_back(s);
}


void SpringBody::clearAllSprings () {
  mSprings.clear();
  _buildDefaultSprings();
}


void SpringBody::_buildDefaultSprings () {
  int c = mPointCount;
  for (int i = 0; i < c; ++i) {
    int n = (i+1 < c ? i+1 : 0);
    addInternalSpring(i, n, mEdgeSpringK, mEdgeSpringDamp);
  }
}


void SpringBody::setEdgeSpringConstants (float edgeSpringK, float edgeSpringDamp) {
  mEdgeSpringK = edgeSpringK;
  mEdgeSpringDamp = edgeSpringDamp;
  // we know that the first n springs in the list are the edge springs
  int c = mPointCount;
  for (int i = 0; i < c; ++i) {
    mSprings[i].springK = edgeSpringK;
    mSprings[i].damping = edgeSpringDamp;
  }
}


void SpringBody::setSpringConstants (int springID, float springK, float springDamp) {
  // index is for all internal springs, AFTER the default internal springs
  int index = mPointCount+springID;
  mSprings[index].springK = springK;
  mSprings[index].damping = springDamp;
}


float SpringBody::getSpringK (int springID) const {
  int index = mPointCount+springID;
  return mSprings[index].springK;
}


float SpringBody::getSpringDamping (int springID) const {
  int index = mPointCount+springID;
  return mSprings[index].damping;
}


void SpringBody::accumulateInternalForces () {
  // internal spring forces
  Vector2 force;
  int i = 0;
  for (SpringList::const_iterator it = mSprings.begin(); it != mSprings.end(); ++it, ++i) {
    const InternalSpring &s = *it;
    PointMass& pmA = mPointMasses[s.pointMassA];
    PointMass& pmB = mPointMasses[s.pointMassB];
    if (i < mPointCount) {
      // spring forces for the edges of the shape can used the cached edge information to reduce calculations
      force = calculateSpringForce(-mEdgeInfo[i].dir, mEdgeInfo[i].length, pmA.Velocity, pmB.Velocity, s.springD, s.springK, s.damping);
    } else {
      // these are other internal springs, they must be fully calculated each frame
      force = calculateSpringForce(pmA.Position, pmA.Velocity, pmB.Position, pmB.Velocity, s.springD, s.springK, s.damping);
    }
    pmA.Force += force;
    pmB.Force -= force;
  }
  // shape matching forces
  if (mShapeMatchingOn) {
    mBaseShape.transformVertices(mDerivedPos, mDerivedAngle, mScale, mGlobalShape);
    for (int i = 0; i < mPointCount; ++i) {
      PointMass& pmA = mPointMasses[i];
      if (mShapeSpringK > 0) {
        if (!mKinematic) {
          force = calculateSpringForce(pmA.Position, pmA.Velocity, mGlobalShape[i], pmA.Velocity, 0.0f, mShapeSpringK, mShapeSpringDamp);
        } else {
          force = calculateSpringForce(pmA.Position, pmA.Velocity, mGlobalShape[i], Vector2::Zero, 0.0f, mShapeSpringK, mShapeSpringDamp);
        }
        pmA.Force += force;
      }
    }
  }
}

}
