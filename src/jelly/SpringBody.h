/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _SPRING_BODY_H
#define _SPRING_BODY_H

#include "Body.h"
#include "InternalSpring.h"


namespace JellyPhysics {

class SpringBody : public Body {
public:
  SpringBody (
    World *w, const ClosedShape &shape, float massPerPoint,
    float edgeSpringK, float edgeSpringDamp,
    const Vector2 &pos, float angleInRadians,
    const Vector2 &scale, bool kinematic);

  SpringBody (
    World *w, const ClosedShape &shape, float massPerPoint,
    float shapeSpringK, float shapeSpringDamp,
    float edgeSpringK, float edgeSpringDamp,
    const Vector2 &pos, float angleinRadians,
    const Vector2 &scale, bool kinematic);

  virtual ~SpringBody ();

  void addInternalSpring (int pointA, int pointB, float springK, float damping);

  void clearAllSprings ();

  void _buildDefaultSprings ();

  void setShapeMatching (bool onoff) { mShapeMatchingOn = onoff; }
  void setShapeMatchingConstants (float springK, float damping) { mShapeSpringK = springK; mShapeSpringDamp = damping; }

  void setEdgeSpringConstants (float edgeSpringK, float edgeSpringDamp);
  void setSpringConstants (int springID, float springK, float springDamp);

  float getSpringK (int springID) const;
  float getSpringDamping (int springID) const;

  void accumulateInternalForces ();

protected:
  typedef std::vector<InternalSpring> SpringList;

  SpringList mSprings;
  bool mShapeMatchingOn;
  float mEdgeSpringK;
  float mEdgeSpringDamp;
  float mShapeSpringK;
  float mShapeSpringDamp;
};

}


#endif
