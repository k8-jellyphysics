/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "AABB.h"

namespace JellyPhysics {


AABB::AABB (const Vector2 &minPt, const Vector2 &maxPt) {
  Min = minPt;
  Max = maxPt;
  Validity = Valid;
}


void AABB::clear () {
  Min = Max = Vector2::Zero;
  Validity = Invalid;
}


void AABB::expandToInclude (const Vector2 &pt) {
  if (Validity == Valid) {
    if (pt.X < Min.X) Min.X = pt.X; else if (pt.X > Max.X) Max.X = pt.X;
    if (pt.Y < Min.Y) Min.Y = pt.Y; else if (pt.Y > Max.Y) Max.Y = pt.Y;
  } else {
    Min = Max = pt;
    Validity = Valid;
  }
}


void AABB::expandToInclude (const AABB &aabb) {
  expandToInclude(aabb.Min);
  expandToInclude(aabb.Max);
}


bool AABB::contains (const Vector2 &pt) const {
  if (Validity == Invalid) return false;
  return (pt.X >= Min.X && pt.X <= Max.X && pt.Y >= Min.Y && pt.Y <= Max.Y);
}


bool AABB::intersects (const AABB &box)const {
  bool overlapX = (Min.X <= box.Max.X && Max.X >= box.Min.X);
  bool overlapY = (Min.Y <= box.Max.Y && Max.Y >= box.Min.Y);
  return (overlapX && overlapY);
}


}
