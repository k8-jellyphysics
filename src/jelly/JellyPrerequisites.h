/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _JELLY_PREREQ_H
#define _JELLY_PREREQ_H

#include <vector>

#include "Vector2.h"


namespace JellyPhysics {

  typedef std::vector<Vector2> Vector2List;

  static const float PI = 3.14159265f;
  static const float TWO_PI = (3.14159265f*2.0f);
  static const float HALF_PI = (3.14159265f*0.5f);
  static const float PI_OVER_ONE_EIGHTY = (3.14159265f/180.0f);
  static const float ONE_EIGHTY_OVER_PI = (180.0f/3.14159265f);

  //static inline float absf (float v) { return (v >= 0.0f) ? v : -v; }
}


#endif
