/*
 * Copyright (c) 2007 Walaber
 * Modified by Ketmar // Invisible Vector
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _VECTOR_TOOLS_H
#define _VECTOR_TOOLS_H

#include <math.h>

#include "JellyPrerequisites.h"
#include "Vector2.h"


namespace JellyPhysics {

static inline Vector2 rotateVector (const Vector2 &vecIn, float angleRadians) {
  Vector2 ret;
  float c = cosf(angleRadians);
  float s = sinf(angleRadians);
  ret.X = (c*vecIn.X)-(s*vecIn.Y);
  ret.Y = (c*vecIn.Y)+(s*vecIn.X);
  return ret;
}

static inline Vector2 rotateVector (const Vector2 &vecIn, float cosAngle, float sinAngle) {
  Vector2 ret;
  ret.X = (cosAngle*vecIn.X)-(sinAngle*vecIn.Y);
  ret.Y = (cosAngle*vecIn.Y)+(sinAngle*vecIn.X);
  return ret;
}

static inline Vector2 reflectVector (const Vector2 &V, const Vector2 &N) {
  return V-(N*(V.dotProduct(N)*2.0f));
}

static inline void reflectVector (const Vector2 &V, const Vector2 &N, Vector2 &vOut) {
  float dot = V.dotProduct(N);
  vOut = V-(N*(2.0f*dot));
}

static inline bool isCCW (const Vector2 &A, const Vector2 &B) {
  Vector2 perp = A.getPerpendicular();
  float dot = B.dotProduct(perp);
  return (dot >= 0.0f);
}

bool lineIntersect (const Vector2 &ptA, const Vector2 &ptB, const Vector2 &ptC, const Vector2 &ptD, Vector2 &hitPt, float &Ua, float &Ub);
bool lineIntersect (const Vector2 &ptA, const Vector2 &ptB, const Vector2 &ptC, const Vector2 &ptD, Vector2 &hitPt);

Vector2 calculateSpringForce (
  const Vector2 &posA, const Vector2 &velA,
  const Vector2 &posB, const Vector2 &velB,
  float springD, float springK, float damping);

Vector2 calculateSpringForce (
  const Vector2 &dir, float length,
  const Vector2 &velA, const Vector2 &velB,
  float springD, float springK, float damping);

static inline float degToRad (float deg) { return deg*(PI_OVER_ONE_EIGHTY); }
static inline float radToDeg (float rad) { return rad*(ONE_EIGHTY_OVER_PI); }

}


#endif
